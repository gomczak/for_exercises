# Generate a list of numbers from 2 to 5.5 in steps of 0.5
# Results have to be decimal type.


from decimal import Decimal


def generate_list_of_numbers(start_number, end_number, step):
    numbers = []
    while start_number <= end_number:
        numbers.append(Decimal(start_number))
        start_number = start_number + step
    return numbers
