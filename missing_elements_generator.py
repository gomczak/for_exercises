# A list containing elements 1-n is given.
# Write a function that will check what it is missing.
# 1-n = [1,2,3,4,5,...,10]
# e.g. n=10
# input: [2,3,7,4,9], 10
# output: [1,5,6,8,10]


def find_missing_elements(start_list, n):
    n = int(n)
    whole_list = set(range(1, n + 1))
    return list(whole_list.difference(set(start_list)))
