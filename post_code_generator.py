# Post code generator
# Takes two strings: "79-900" i "80-155" and returns a list of codes between them


def generate_post_code(post_code1, post_code2):
    try:
        full_post_code1 = int(post_code1.replace('-', ''))
        full_post_code2 = int(post_code2.replace('-', ''))
        codes = []
        if len(post_code1) != 6 or len(post_code2) != 6 \
                or '-' not in post_code1[2] or '-' not in post_code2[2] \
                or full_post_code1 == full_post_code2 or full_post_code1 > full_post_code2 \
                or post_code1[:2] == '00' or post_code2[:2] == '00':
            raise Exception('Check input. Post code have to be five digits and divided "-" after second digit.'
                            '\nThere is no post code start with "00"'
                            '\nPlease start with lower value. '
                            '\nIf input is equal there is no post codes between.')
    except AttributeError:
        print('Post codes have to be strings. Check input.')

    except ValueError:
        print('Check input. Post code have to be five digits and divided "-" after second digit. '
              '\nLetters are not allowed.')
    else:
        for post_code in range(full_post_code1 + 1, full_post_code2):
            first_part_of_post_code = post_code // 1000
            second_part_of_post_code = post_code % 1000
            if second_part_of_post_code != 000:
                codes.append('{:02}-{:03}'.format(first_part_of_post_code, second_part_of_post_code))
        return codes
